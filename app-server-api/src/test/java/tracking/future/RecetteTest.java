package tracking.future;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RecetteTest {

    Recette recette;

    @Before
    public void setUp() {
        this.recette = new Recette();
        this.recette.setId(1L);
        this.recette.setTitre("Omelette champi et herbe de provence");
        this.recette.setDuree(15);
        this.recette.setDescription("Blablablabla");
    }

    @Test
    public void getId() {
        assertEquals(new Long(1), recette.getId());
    }

    @Test
    public void getName() {
        assertEquals("Omelette champi et herbe de provence", recette.getTitre());
    }

    @Test
    public void getDuree() { assertEquals(15, recette.getDuree()); }
}
