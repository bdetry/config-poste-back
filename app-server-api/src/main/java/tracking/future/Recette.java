package tracking.future;

import lombok.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "epsi_tp_cf_recette")
@EntityListeners(AuditingEntityListener.class)
@Data
// @NoArgsConstructor
public class Recette {
    @Id
    @GeneratedValue
    private long id;

    @Column
    private @NonNull String titre;

    @Column
    private int duree;

    @Column
    private String description;

    public Recette(long i, String titre, int duree, String description) {
        super();
        this.id = i;
        this.titre = titre;
        this.duree = duree;
        this.description = description;
    }

    public Recette() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public int getDuree() {
        return duree;
    }

    public void setDuree(int duree) {
        this.duree = duree;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "[num : " + id + ", titre : " + titre + ", duree : " + duree + ", description : " + description + "]";
    }
}
