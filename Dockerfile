FROM openjdk:8
ARG bddpassword
ENV env_bddpassword=$bddpassword
VOLUME /tmp
RUN mkdir -p /app-server-main/src/main/resources/
ADD app-server-main/target/*.jar tracking-future-app.jar
ADD app-server-main/src/main/resources/Blank_A4_Landscape.jrxml /app-server-main/src/main/resources/Blank_A4_Landscape.jrxml
EXPOSE 8080
ENTRYPOINT ["java","-Dspring.datasource.password=${env_bddpassword}","-jar","tracking-future-app.jar"]

