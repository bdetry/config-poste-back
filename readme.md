Compiler l'application :
```bash
./mvnw clean package -Dspring.datasource.password=
```

Construction du container Docker :
```bash
docker-compose build --build-arg -Dspring.datasource.password=
```

Lancer le container Docker :
```bash
docker-compose up
```

