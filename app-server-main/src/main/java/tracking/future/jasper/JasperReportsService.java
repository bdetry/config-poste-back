package tracking.future.jasper;


import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.stereotype.Service;
import tracking.future.Recette;
import tracking.future.TrackingFutureApplication;

import java.io.ByteArrayOutputStream;
import java.util.*;
import java.util.logging.Logger;

@Service
public class JasperReportsService {

    private static final Logger logger = Logger.getLogger(TrackingFutureApplication.class.getName());

    public byte[] exportPdf(List<Recette> recettes) {
        try {
            logger.info(recettes.toString());

            String reportPath = "app-server-main/src/main/resources/";

            // Compile the Jasper report from .jrxml to .japser
            JasperReport jasperReport = JasperCompileManager.compileReport(reportPath + "Blank_A4_Landscape.jrxml");

            // Get your data source
            JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(recettes);

            // Add parameters
            Map<String, Object> parameters = new HashMap<>();

            parameters.put("createdBy", "Websparrow.org");

            // Fill the report
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
                    jrBeanCollectionDataSource);

            // Export the report to a PDF file
            final ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            JasperExportManager.exportReportToPdfFile(jasperPrint, reportPath + "Emp-Rpt.pdf");
            JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);

            System.out.println("Report successfully generated @path= " + reportPath);

            return outStream.toByteArray();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
