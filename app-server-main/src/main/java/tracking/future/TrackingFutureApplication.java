package tracking.future;

import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.core.Ordered;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

@SpringBootApplication
@EnableJpaAuditing
public class TrackingFutureApplication {

    private static final Logger logger = Logger.getLogger(TrackingFutureApplication.class.getName());

    public static void main(String[] args) {
        SpringApplication.run(TrackingFutureApplication.class, args);
    }

    @Bean
    ApplicationRunner init(RecetteRepository repository) {
        return args -> {
            List<Recette> recettes = Recettes.getBestRecettes();
            for (Recette recette : recettes) {
                repository.save(recette);
            }
            repository.findAll().forEach(recette -> logger.info(recette.getTitre()));
        };
    }

    @Bean
    public FilterRegistrationBean<CorsFilter> simpleCorsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.setAllowedOrigins(Collections.singletonList("*"));
        config.setAllowedMethods(Collections.singletonList("*"));
        config.setAllowedHeaders(Collections.singletonList("*"));
        source.registerCorsConfiguration("/**", config);
        FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<>(new CorsFilter(source));
        bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
        return bean;
    }
}

