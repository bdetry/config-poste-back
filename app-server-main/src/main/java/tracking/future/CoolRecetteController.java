package tracking.future;

import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import tracking.future.jasper.JasperReportsService;

import java.util.*;
import java.util.stream.Collectors;

@RestController
public class CoolRecetteController {
    private RecetteRepository repository;
    private JasperReportsService reportService;

    public CoolRecetteController(RecetteRepository repository) {
        this.repository = repository;
        this.reportService = new JasperReportsService();
    }

    @GetMapping("/cool-recettes")
    @CrossOrigin(origins = "*")
    public Collection<Recette> coolRecettes() {
        return repository.findAll().stream()
                .filter(this::hasTitre)
                .collect(Collectors.toList());
    }

    @GetMapping("/export-recette/{id}")
    public ResponseEntity<byte[]> report(@PathVariable(required = true) Long id) {
        Recette recette = repository.getOne(id);
        List<Recette> recettesList = Collections.singletonList(recette);
        // Map<String, Object> params = new HashMap<>();
        int idRecette = 1;
        // params.put("id", idRecette);
        byte[] bytes = reportService.exportPdf(recettesList);
        return ResponseEntity
                .ok()
                // Specify content type as PDF
                .header("Content-Type", "application/pdf; charset=UTF-8")
                // Tell browser to display PDF if it can
                .header("Content-Disposition", "inline; filename=\"recette-" + idRecette + ".pdf\"")
                .body(bytes);
    }

    private boolean hasTitre(Recette recette) {
        return !StringUtils.isEmpty(recette.getTitre());
    }
}
